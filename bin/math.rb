module Mymath

	MEAN = lambda { |data| (data.reduce(:+))/(data.size)}
	
	MEDIAN = lambda do |data| 
		dataSorted = data.sort 
		return dataSorted[(dataSorted.size-1)/2] if dataSorted.size.odd?
		return ((dataSorted[dataSorted.size/2-1]+dataSorted[dataSorted.size/2]))/2.0 
	end



end