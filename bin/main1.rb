require './database.rb'
require './bin.rb'
require './math.rb'

def printBin(data)
	data.each do |ele|
		print "Bin " + (data.index(ele)+1).to_s + " : "
		print ele.to_s + "\n"
	end
	puts "\n"
end

def main
	db_name = "products"
	tb_name = "products"
	bin_depth = 4

	db = DBHelper.new(db_name,tb_name)
	prices = db.selectColoumn("price")

	bin = Bin.new(prices,bin_depth)

	puts "\n\nMean :"
	printBin(bin.aggregate(Mymath::MEAN))

	puts "\n\nMedian :"
	printBin(bin.aggregate(Mymath::MEDIAN))

	puts "\n\nBoundary :"
	printBin(bin.boundary)
end

main()

