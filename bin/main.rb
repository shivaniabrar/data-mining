require 'sqlite3'

nbins = 4
db_name = "products"
tb_name = "products"
db_selectall = "select * from "
bins = []

db = SQLite3::Database.new db_name
data = db.execute(db_selectall+tb_name)

boundary = lambda do |arr|
	arr.map do |i|  
		if (i-arr.min)<(arr.max-i)
			i = arr.min
		else
			i = arr.max
		end
	end
end  

def printBoundary(bins,boundary)
	bins.each do |bin|
		print "Bins " + (bins.index(bin)+1).to_s + " : "
		print boundary.call(bin).to_s + "\n"
	end
end

def printAggregrate(bins,aggrftn)
	bins.each do |bin|
		print "Bins " + (bins.index(bin)+1).to_s + " : "
		bin.size.times{ print aggrftn.call(bin).to_i.to_s + " " }
		puts ""
	end
end

price = data.flatten.select {|i| i.class.to_s == "Float" or i.class.to_s == "Fixnum"}.sort

(0..price.size-1).step(nbins).each do |iprice|
	bins << price[iprice..(iprice+nbins-1)]
end

mean = lambda { |arr| (arr.reduce(:+))/(arr.size)}

median = lambda do |arr| 
	arrtmp = arr.sort 
	return arrtmp[(arr.size-1)/2] if arr.size.odd?
	return ((arrtmp[arr.size/2-1]+arrtmp[arr.size/2]))/2.0 
end

puts "Mean :"
printAggregrate(bins,mean)
puts "Median :"
printAggregrate(bins,median)
puts "Boundary :"
printBoundary(bins,boundary)