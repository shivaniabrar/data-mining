require 'sqlite3'

class DBHelper
	def initialize(db_name,tb_name)
		@db = SQLite3::Database.new db_name
		@tb = tb_name
	end

	def selectall()
		return @db.execute("select * from "+ @tb +" ;")
	end

	def selectColoumn(colName)
		return @db.execute("select "+colName+" from "+ @tb +" ;").flatten
	end
end