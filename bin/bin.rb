class Bin

	def initialize(data,binDepth)
		@binDepth = binDepth
		@data = data
		@bins = []
		initBins
	end

	def initBins
		(0..@data.size-1).step(@binDepth).each do |item|
			@bins << @data[item..(item+@binDepth-1)]
		end
	end

	def aggregate(aggrfunct)
		results = []
		@bins.each do |bin|
		    result = []
			bin.size.times{ result << aggrfunct.call(bin).to_i.to_s}
			results << result
		end
		return results
	end

	def boundary
		result = []
		@bins.each do |bin|
			result << bin.map do |element|  
				if (element-bin.min)<(bin.max-element)
					element = bin.min
				else
					element = bin.max
				end
			end
		end
		return result
	end  
	
	def to_s
		@bins.each do |bin|
			print "Bin " + (bins.index(bin)+1).to_s + " : "
			print bin.to_s + "\n"
		end
	end


end